module "VPC" {
  source = "./VPC"
}
module "Server" {
    source = "./Server"
    sn = module.VPC.pub_sub
    sg = module.VPC.SecuityGroup
}