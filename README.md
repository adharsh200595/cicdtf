# Project Description

This project utilises GitLab CI/CD (Continuous Integration/Continuous Deployment) pipelines along with Terraform to automate the provisioning and management of infrastructure on AWS (Amazon Web Services). The project is orchestrated through a GitLab CI/CD pipeline defined in a GitLab CI configuration file (`gitlab-ci.yml`).

## Pipeline Structure

The pipeline is structured into four stages: validate, plan, apply, and destroy, each representing a distinct phase of the infrastructure lifecycle.

1. **Validate Stage**:
   - In the validate stage, the Terraform configuration files are validated to ensure their syntactic correctness and adherence to best practices.

2. **Plan Stage**:
   - Following successful validation, the plan stage generates an execution plan based on the Terraform configuration. This plan details the actions Terraform will take to achieve the desired infrastructure state.

3. **Apply Stage**:
   - Upon manual approval, the apply stage executes the Terraform plan, provisioning or updating the infrastructure resources on AWS as specified in the Terraform configuration files.

4. **Destroy Stage**:
   - The destroy stage provides a manual option to tear down the provisioned infrastructure, effectively decommissioning all AWS resources managed by Terraform.

## Docker Image
The project utilises a Docker image (`registry.gitlab.com/gitlab-org/terraform-images/stable:latest`) as the runtime environment for executing Terraform commands. The Docker image includes Terraform and other necessary dependencies.

## Environment Variables
Key environment variables such as AWS access keys (`AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY`) and the default AWS region (`AWS_DEFAULT_REGION`) are securely managed as GitLab CI/CD variables, ensuring sensitive information is not exposed in the GitLab CI configuration file.

## Conclusion
This project demonstrates an automated approach to infrastructure management, enabling efficient provisioning, maintenance, and decommissioning of AWS resources through Terraform and GitLab CI/CD pipelines.
