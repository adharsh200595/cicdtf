resource "aws_vpc" "Project_vpc" {
    cidr_block = "15.0.0.0/16"
    enable_dns_hostnames = true
    enable_dns_support = true
    tags = {
      Name = "Project_vpc"
    }
}
resource "aws_subnet" "Subnet_proj" {
    vpc_id = aws_vpc.Project_vpc.id
    cidr_block = "15.0.1.0/24"
    map_public_ip_on_launch = true
    availability_zone = "ap-south-1a"
    tags = {
      Name = "Subnet_proj"
    }
}
resource "aws_security_group" "sg_server" {
    vpc_id = aws_vpc.Project_vpc.id
    name = "sg_server"
    description = "My Project SG"
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
  
}