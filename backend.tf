terraform {
  backend "s3" {
    bucket = "terraformstateadharssh"
    key    = "State"
    region = "ap-south-1"
    dynamodb_table = "terraform-lock"
  }
}
