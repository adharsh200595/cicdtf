resource "aws_instance" "WebServer" {
    ami = "ami-009e46eef82e25fef"
    instance_type = "t2.micro"
    subnet_id = var.sn
    security_groups = [var.sg]
    tags = {
      Name = "MyWebServer"
    }
}